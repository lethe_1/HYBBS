<?php
namespace Action;
use HY\Action;

class IndexAction extends HYBBS {
	public function __construct(){
		parent::__construct();
		{hook a_index_init}
		$left_menu = array('index'=>'active','forum'=>'');
		$this->v("left_menu",$left_menu);
	}
	public function Index(){
		{hook a_index_index_1}

		$this->v('title',$this->conf['title']);

		$pageid=intval(X('get.pageid')) or $pageid=1;
		$data = S("Thread")->select("*",array("ORDER"=>"id DESC","LIMIT" => array(($pageid-1) * 10, 10)));
		$user_tmp = array();
		$User = M("User");
		foreach ($data as  &$v){
			if(empty($user_tmp[$v['uid']])){
				$user_tmp[$v['uid']] = $User->id_to_user($v['uid']);

			}

			

			$v['user'] = $user_tmp[$v['uid']];
			$v['summary'] = strip_tags($v['summary']);
			$v['atime'] = humandate($v['atime']);
			$v['avatar']=$this->avatar($v['user']);
			if(!empty($v['img'])){
				$v['image']=explode(",", $v['img']);
				$v['image_count']=count($v['image'])-1;
			}

		}

		$count = M("Count")->xget('thread');
		$count = (!$count)?1:$count;
		$page_count = ($count % 10 != 0)?(intval($count/10)+1) : intval($count/10);


		{hook a_index_index_v}
		$this->v("pageid",$pageid);
		$this->v("page_count",$page_count);
		$this->v("data",$data);
		$this->display('index_index');
	}
	public function test(){
		$sql = S("Plugin");



	}
	{hook a_index_fun}
}
