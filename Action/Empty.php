<?php
namespace Action;
use HY\Action;

class EmptyAction extends HYBBS {
    public function __construct(){
        parent::__construct();
        {hook a_empty_init}
    }
    public function index(){
        {hook a_empty_index_v}
        echo '你是不是来错地方了?';
    }
    public function _empty(){
        {hook a_empty_empty_v}
        echo 'Empty->Empty()';
    }
    {hook a_empty_fun}
}
