<?php
namespace Action;
use HY\Action;

class SearchAction extends HYBBS {
    public function __construct() {
		parent::__construct();
        {hook a_search_init}
    }
    public function _empty(){
        $key = METHOD_NAME; //搜索关键字
        {hook a_search_empty_1}
        $this->v('title',$key.' 搜索');


        //echo $key;
        $pageid=intval(X('get.pageid')) or $pageid=1;
		$data = S("Thread")->select("*",array("title[~]" => $key,"ORDER"=>"id DESC","LIMIT" => array(($pageid-1) * 10, 10)));
        {hook a_search_empty_2}
        $user_tmp = array();
		$User = M("User");
		foreach ($data as  &$v){
			if(empty($user_tmp[$v['uid']])){
				$user_tmp[$v['uid']] = $User->id_to_user($v['uid']);

			}

			$v['user'] = $user_tmp[$v['uid']];
			$v['summary'] = strip_tags($v['summary']);
			$v['atime'] = humandate($v['atime']);
			$v['avatar']=$this->avatar($v['user']);
		}
        {hook a_search_empty_3}
		$count = M("Count")->xget('thread');
		$count = (!$count)?1:$count;
		$page_count = ($count % 10 != 0)?(intval($count/10)+1) : intval($count/10);

        $left_menu = array('index'=>'active','forum'=>'');
		$this->v("left_menu",$left_menu);

        {hook a_search_empty_v}
		$this->v("pageid",$pageid);
		$this->v("page_count",$page_count);
		$this->v("data",$data);
		$this->display('index_index');

        //print_r($data);
    }
    {hook a_search_fun}
}
