<?php
namespace Action;
use HY\Action;
class HYBBS extends Action {
    public $_user=array();
    public $_login=false;
    public $_theme;
    public $_forum=array();
    public $fc;
    public $conf;
    {hook a_hybbs_var}

    public function __construct(){


        {hook a_hybbs_init}

        $conf = file(CONF_PATH . 'conf.php');
        $this->conf = json_decode($conf[1],true);
        $this->v("conf",$this->conf);


        if(empty(C('DOMAIN_NAME')))
            return header("location: /install");

        {hook a_hybbs_init_1}
        $this->_theme = $this->view = $this->conf['theme'];
        define('THEME_NAME',$this->_theme);
        define('WWW',C('DOMAIN_NAME'));

        //初始化用户状态
        $this->init_user();
        define("IS_LOGIN",$this->_login);
        {hook a_hybbs_init_2}

        //生成板块缓存 File
        $this->fc = L("Filecache");
        $forum = $this->fc->get(md5("forum".C("MD5_KEY")));
        if(empty($forum) || DEBUG){ //调试模式 每次都生成缓存
            $forum = S("Forum")->select("*");
            $this->fc->set(md5("forum".C("MD5_KEY")),$forum);
        }

        foreach ($forum as $k => $v) {
            $this->_forum[intval($v['id'])] = $v;
        }
        //print_r($this->_forum);
        {hook a_hybbs_init_v}
        $this->v("forum",$this->_forum);

    }
    public function init_user(){
        $cookie = cookie("HYBBS_HEX");
        if(!empty($cookie)){
            $UserLib = L("User");
            $user = $UserLib->get_cookie($cookie);


            if(!empty($user)){

                if(isset($user['id']) && isset($user['user']) && S("User")->has(array('AND'=>array('id'=>$user['id'],'user'=>$user['user'])))){
                    $user['avatar'] = $this->avatar($user['user']);
                    //print_r($user);
                    $this->_user = $user;

                    $this->_login=true;
                    $this->v('user',$this->_user);

                }
            }
        }


    }
    public function message($msg){
        {hook a_hybbs_message}
        $this->v('title',$msg.' - 错误提示');
        $this->v("msg",$msg);
        $this->display('message');
    }
    //获取用户头像
    public function avatar($user){
        {hook a_hybbs_avatar}
        $path = INDEX_PATH . 'upload/avatar/' . md5($user.C("MD5_KEY"));
        $path1 = '/upload/avatar/' . md5($user.C("MD5_KEY"));
        if(!file_exists($path.'.jpg'))
            return array(
                'a'=>'/public/images/user.gif',
                'b'=>'/public/images/user.gif',
                'c'=>'/public/images/user.gif',
            );
        return array(
            "a"=>$path1."-a.jpg",
            "b"=>$path1."-b.jpg",
            "c"=>$path1."-c.jpg"
        );
    }
    {hook a_hybbs_fun}
}
