<?php
namespace Action;
use HY\Action;

class ForumAction extends HYBBS {
    public function __construct(){
		parent::__construct();

		$left_menu = array('index'=>'','forum'=>'active');
		$this->v("left_menu",$left_menu);
        {hook a_forum_init}
	}
    public function index(){
        {hook a_forum_index_v}
        $this->v("title","板块分类首页");
        $data = S("Forum")->select("*");
        $this->v("data",$data);
        $this->display('forum_index');
    }
    public function _empty(){

        {hook a_forum_empty_1}
        $id = intval(METHOD_NAME);

        //print_r($this->_forum);
        //var_dump(isset($this->_forum[$id]));
        if(!isset($this->_forum[$id]))
            return $this->message("没有此分类!");

        $pageid=intval(X('get.pageid')) or $pageid=1;
		$data = S("Thread")->select("*",array("fid"=>$id,"ORDER"=>"id DESC","LIMIT" => array(($pageid-1) * 10, 10)));
        {hook a_forum_empty_2}
		$user_tmp = array();
		$User = M("User");
		foreach ($data as  &$v){
			if(empty($user_tmp[$v['uid']])){
				$user_tmp[$v['uid']] = $User->id_to_user($v['uid']);

			}

			$v['user'] = $user_tmp[$v['uid']];
			$v['summary'] = strip_tags($v['summary']);
			$v['atime'] = humandate($v['atime']);
			$v['avatar']=$this->avatar($v['user']);

            if(!empty($v['img'])){
				$v['image']=explode(",", $v['img']);
				$v['image_count']=count($v['image'])-1;
			}
            
		}

		$count = M("Count")->xget('thread');
		$count = (!$count)?1:$count;
		$page_count = ($count % 10 != 0)?(intval($count/10)+1) : intval($count/10);

        {hook a_forum_empty_v}
        $this->v("title",$this->_forum[$id]['name']);
		$this->v("pageid",$pageid);
		$this->v("page_count",$page_count);
		$this->v("data",$data);
		$this->display('index_index');
    }
    {hook a_forum_fun}
}
