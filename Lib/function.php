<?php
//计算时间间隔
function humandate($timestamp) {
	$seconds = $_SERVER['REQUEST_TIME'] - $timestamp;
	if($seconds > 31536000) {
		return date('Y-n-j', $timestamp);
	} elseif($seconds > 2592000) {
		return floor($seconds / 2592000).'月前';
	} elseif($seconds > 86400) {
		return floor($seconds / 86400).'天前';
	} elseif($seconds > 3600) {
		return floor($seconds / 3600).'小时前';
	} elseif($seconds > 60) {
		return floor($seconds / 60).'分钟前';
	} else {
		return $seconds.'秒前';
	}
}

function get_plugin_inc($plugin_name){

	if(!is_file(PLUGIN_PATH . "{$plugin_name}/inc.php"))
		return false;
	//echo PLUGIN_PATH . "{$plugin_name}/inc.php";
	$path = PLUGIN_PATH . "{$plugin_name}/inc.php";
	$file = file($path);
	return json_decode($file[1],true);
}
//获取插件安装状态
function get_plugin_install_state($plugin_name){
	if(!is_file(PLUGIN_PATH . "{$plugin_name}/install"))
		return false;
	return true;
}
//删除目录
function deldir($dir) {

  $dh=opendir($dir);
  while ($file=readdir($dh)) {
    if($file!="." && $file!="..") {
      $fullpath=$dir."/".$file;
      if(!is_dir($fullpath)) {
          unlink($fullpath);
      } else {
          deldir($fullpath);
      }
    }
  }

  closedir($dh);
  if(rmdir($dir)) {
    return true;
  } else {
    return false;
  }
}
