
$(function(){
  $iWidth = $(window).width();
  $iHeight= $(window).height();
  $body = $('body');
  //size_init();
  //resp();
  $(".qj-box").fadeIn(300);

 $(".qj-menu").click(function(){
    $(".qj-menu").removeClass("active");
    $(this).addClass("active");
  });


})

// $(window).resize(function() {
//   $iWidth = $(window).width();
//   $iHeight= $(window).height();
//   size_init();
//   resp();
// });


function size_init(){
  if($iHeight < 643){
    $(".qj-menu").height(75);
    $(".qj-menu-title").hide();
  }else{
    $(".qj-menu").height(100);
    $(".qj-menu-title").show();
  }

  if($iWidth < 1300){
    $(".qj-left").width(80);
  }else{
    $(".qj-left").width(130);
  }
  if($iWidth < 801){
    $(".c-left,.match-box,.match-html").css("margin-right","0px");


    $(".c-right").hide();
  }else{
    $(".c-left,.match-box,.match-html").css("margin-right","250px");
    $(".c-right").show();
  }
  //$(".match-box").width($(".c-left").width()+30);
  //$(".match-html").width($(".c-left").width()-130);
  //console.log($(".c-left").width()+30);

}
function resp(){
  $(".qj-right").width($iWidth - $(".qj-left").width() - 30);
  $(".qj-right").css("left",$(".qj-left").width()+"px");
}
function tp(type,id,obj){

    $.ajax({
      url: '/post/vote.html',
      type:"POST",
      cache: false,
      data:{type:type,id:id},
      dataType: 'json'
    }).then(function(data) {
      if(obj != "" || obj != undefined || obj != null){
        if(data.info==true){
          $(obj).attr("class","label label-success");
          $(obj).find("span").text(parseInt($(obj).text())+1);
        }
        else{
          $(obj).attr("class","label label-danger");
        }

      }

    }, function() {


    });
}
function max(obj,url){
  obj.attr('src',url);
  obj.parent().parent().find('span').hide();
  obj.parent().parent().find('p').hide();
  obj.parent().show();
}
function max1(obj){
  obj.parent().hide();
  obj.parent().parent().find('span').show();
  obj.parent().parent().find('p').show();
}
function imgxz(obj){
  $id = obj.attr('xz');
  if($id=="0"){
    obj.css('transform','translate(0px, 24px) rotate(810deg) scale(1, 1)');
    obj.attr('xz',"1");
  }else if($id=="1"){
    obj.css('transform','translate(0px, 0px) rotate(900deg) scale(1, 1)');
    obj.attr('xz',"2");
  }else if($id=="2"){
    obj.css('transform','translate(0px, 24px) rotate(990deg) scale(1, 1)');
    obj.attr('xz',"3");
  }else if($id=="3"){
    obj.css('transform','translate(0px, 0px) rotate(1080deg) scale(1, 1)');
    obj.attr('xz',"0");
  }
}

//删除主题
function del_thread(id,type){
    if(confirm("真的要删除吗?\r\n\r\n 评论过也会随着主题删除!")){
        $.ajax({
            url: www+"/"+type+"/del.html",
            type:"POST",
            cache: false,
            data:{
             id:id
            },
            dataType: 'json'
        }).then(function(e) {
            alert(e.info);
        }, function() {
            alert('请尝试重新提交');
        });

    }else{

    }
}

function thread_top(id,type,top){
    if(confirm("真的要这么做?")){
        $.post( www+"/thread/top.html",{id:id,type:type,top:top},function(e){
                alert(e.info);
                if(e.error){

                    window.location.reload()
                }
            }
            ,'json'
        )
    }
}
