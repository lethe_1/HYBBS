//<?php

public function forumg(){
    $this->menu_action['forumg'] = ' active open';
    $this->v("menu_action",$this->menu_action);

    if(IS_POST){
        $id = X("post.id");
        $user = X("post.user");
        S("Forum")->update(array(
            'forumg'=>$user
        ),array(
            'id'=>$id
        ));
        return $this->mess('修改完成');

    }
    if(IS_AJAX){
        $id = X("get.id");
        if($id > -1){
            $user = S("Forum")->find("forumg",array(
                'id'=>$id
            ));
            $this->v("user",$user);
            $this->v("id",$id);
            C("DEBUG_PAGE",false);
            return $this->display("plugin.hy_forum_group::ajax_forum");
        }
    }


    $Forum = S("Forum");
    $data = $Forum->select("*");

    $User = M("User");
    foreach ($data as &$v) {
        $tmp = explode(",",$v['forumg']);
        if(!count($tmp))
            continue;
        $v['user'] = array();
        foreach ($tmp as $vv) {
            $v['user'][]=$User->id_to_user(intval($vv));

        }
        //$v['user'] = $user;
        unset($tmp);
    }

    $this->v("data",$data);
    $this->display('plugin.hy_forum_group::forumg');
}
