//<?php

$id = intval(X("post.id"));

$data = M("Thread")->read($id);
if(empty($data))
    return $this->json(array('error'=>false,'info'=>'没有该文章'));

$arr = explode(",",$this->_forum[$data['fid']]['forumg']);

if(!array_search($this->_user['id'],$arr) && $this->_user['group'] != C("ADMIN_GROUP"))
    return $this->json(array('error'=>false,'info'=>'你没有权限操作这个主题'));


$type = X("post.type");
$top = X("post.top");
if($top < 0 || $top > 2)
    return $this->json(array('error'=>false,'info'=>'参数出错'));

if($top == 2){
    if($this->_user['group'] != C("ADMIN_GROUP"))
        return $this->json(array('error'=>false,'info'=>'你没有权限全站置顶'));
}
S("Thread")->update(array(
    'top'=>($type=='on') ? $top : 0
),array(
    'id'=>$id
));

return $this->json(array('error'=>true,'info'=>'操作成功'));
