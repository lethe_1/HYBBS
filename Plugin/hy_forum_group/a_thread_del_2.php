//<?php
$arr = explode(",",$this->_forum[$t_data['fid']]['forumg']);

if(!array_search($this->_user['id'],$arr) && $t_data['uid'] != $this->_user['id'])
    return $this->json(array('error'=>false,'info'=>'你没有权限操作这个主题'));


$Thread->del($id);

if($t_data['posts']){ //存在评论
    $Post = M('Post');
    $Post->del_thread_all_post($id);
}

return $this->json(array('error'=>true,'info'=>'删除成功'));
