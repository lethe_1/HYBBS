<?php
namespace Model;
use HY\Model;

class ThreadModel extends Model {
    //通过文章ID 获取文章
    public function read($id){
        return $this->find("*",array(
            'id'=>$id
        ));
    }
    //删除文章
    public function del($id){
        $this->delete(array(
            'id'=>$id
        ));
    }
    // 赞数操作 踩操作 评论数量操作
    public function update_int($id,$key,$type = "+",$size = 1){
        $key .= ($type=='+') ? '[+]' : '[-]';
        $this->update(array(
            $key=>$size
        ),array(
            'id'=>$id
        ));
    }





}
